# Recommendation system using ML

## Node.js App

- Recommendation endpoint

```GET \recommendations\userID\sessionID```

Serves K (currently 3) recommendations from either Collaborative Filtering or Neural Network recommender model.
The model is chosen randomly and assigned to a cookie- this enables A/B testing.

## Configuration

### To launch a model training in a Docker container:

- Create a model training script with path model/model_training.py

This can be a converted Jupyter Notebook script, but **be careful** for commands ! executed in Notebook, these need to be deleted bbefore launching in a docker.

- Build the Dockerfile (in the main directory):

```docker build --tag model_training .```

- Run the Docker app:

```docker run --name model_training model_training```