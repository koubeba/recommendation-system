const functions = require('firebase-functions');
const express   = require('express');
const app       = require('./app');
const cors      = require('cors');

app.use(cors({origin: true}));
const api = functions.https.onRequest(app);

module.exports = {
    api
}