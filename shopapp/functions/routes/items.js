var express = require('express');
var router = express.Router();

/* GET items listing. */
router.get('/', function(req, res, next) {
    itemIds = []
    var db = req.app.locals.db;
    db.collection("products").get().then(function(colRef) {
        colRef.forEach(function(document) {
            itemIds.push(document.id);
        });
        res.send(itemIds);
    });
});

/*POST item view */
router.post('/:itemId/view', function(req, res, next) {
    var userId      = req.body.userId;
    var sessionId   = req.body.sessionId;
    res.send('View an item with id ' + req.params["itemId"] + ' for user with id ' + userId + ' for session with id ' + sessionId);
})

/*POST item purchase */
router.post('/:itemId/buy', function(req, res, next) {
    var userId      = req.body.userId;
    var sessionId   = req.body.sessionId;
    res.send('Buy an item with id ' + req.params["itemId"] + ' for user with id ' + userId + ' for session with id ' + sessionId);
})

module.exports = router;
