var express = require('express');
var router  = express.Router();
var spawn   = require('child_process').spawn;
var path    = require('path');
const axios  = require('axios');
const ab     = require('express-ab');

var modelsTest = ab.test('model-test');

/* GET a recommendation for the user. */
router.get('/:userId/:sessionId', modelsTest('NN'), function NN(req, res) {
    var db              = req.app.locals.db;
    var userId          = req.params["userId"];
    var sessionId       = req.params["sessionId"];
    var userProducts    = [];

    db.collection("user_sessions").doc(userId).get().then(function(userDoc) {
        if (userDoc.exists) {
            var sessionData = userDoc.get(sessionId);
            if (sessionData) {
                userProducts = String(sessionData).split(",");
                var data = {
                    "viewed_products": userProducts
                };
            } else {
                var data = {
                    "viewed_products": []
                };
            }
            axios.post("https://blooming-retreat-52678.herokuapp.com/NNrecommendations", data)
                 .then((recomRes) => {
                var recommendationData = recomRes.data;
                recommendationData['model'] = 'NN';
                res.status(200).send(recommendationData);
            }).catch((err) => {
                res.status(err.status).send(`Recommendation API error: ${err.data}`)
            });
        } else {
            res.status(404).send(`No user with ID ${req.params["userId"]}`);
        }
    });
});

router.get('/:userId/:sessionId', modelsTest('CF'), function CF(req, res) {
    var db              = req.app.locals.db;
    var userId          = req.params["userId"];
    var userProducts    = [];
    console.log("hi")

    db.collection("user_products").doc(userId).get().then(function(doc) {
        console.log("db")
        if (doc.exists) {
            userProducts = String(doc.get("products")).split(",");
            var script_path = path.join(__dirname, '../prediction/model.py');
            var data = {
                "viewed_products": userProducts,
                "model": 'CF'
            };
            axios.post("https://blooming-retreat-52678.herokuapp.com/CFrecommendations", data)
            .then((recomRes) => {
                var recommendationData = recomRes.data;
                recommendationData['model'] = 'CF';
                res.status(200).send(recommendationData);
            }).catch((err) => {
                res.status(err.status).send(`Recommendation API error: ${err.data}`)
            });
        } else {
            res.status(404).send(`No user with ID ${req.params["userId"]}`);
        }
    });
});

module.exports = router;
