var assert  = require('assert');
var app     = require('../app');

var chai    = require('chai');
var chaiHttp= require('chai-http');
var should  = chai.should();

chai.use(chaiHttp);

describe('AB test', function() {

    describe('GET recommendation', function() {
        it ('should return recommendation for a valid user', function(done) {
            var userId = 111;
            chai.request(app)
                .get(`/recommendations/${userId}`)
                .end(function(err, res) {
                    console.log(res.body["model"]);
                    var bodyArray = res.body['recommendations'];
                    bodyArray.should.be.a('array');
                    done();
                });
        }, 5000);
    });

});

function delete_cookie( name ) {
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}